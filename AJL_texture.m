clear all; close all; sca;

% Specify size of image in centimeters
cmImage = 66;  

% Dimensions of screen, so we can create image in cm
scr_Num = max(max(Screen('Screens')));
[w_pix, h_pix]=Screen('windowsize', scr_Num);
[w_mm, h_mm]=Screen('DisplaySize', scr_Num);

% calculate the number of pixels per cm
pixpercm = mean([w_pix/w_mm h_pix/h_mm])*10;

% the size of the image in pixels
imagesize = round(cmImage * pixpercm);

% here we create a window to draw the texture into. The texture is bigger
% than the screen, but that shouldn't matter (the size of a window is not
% limited to the screen). We multisample the window to sort out aliasing.
% This is the window that we will read the texture out from at the end.
[tex,rect]=Screen('OpenWindow',0, 128, [0 0 imagesize imagesize], [], [], 0, 4);

% this is the maximum sizes of the ovals in pixels (with minimum being 1)
maxax = 450;

% number of ovals
nOvals = 1000;

for iOval=1:nOvals,

    % these are the random sizes for the two axes
    for i=1:2, ax(i) = rand(1) * maxax; end
    
    % the color of the oval being drawn
    color = floor(rand(1) * 255);
    
    % random rotation
    randRotation = floor(rand(1) * 360);
    
    % random position in the image
    randPosition = [rand(1)*imagesize,rand(1)*imagesize];
 
    % create the rectangle that defines the size of the oval
    rect = [0 0 ax(1) ax(2)];
    
    % push the current matrix stack (OpenGL = magic?)
    Screen('glPushMatrix', tex);
    
    % now translate and rotate the matrix by random amounts
    Screen('glTranslate', tex, randPosition(1), randPosition(2));
    Screen('glRotate', tex, randRotation);
    
    % draw the actual oval
    Screen('FillOval', tex, color,rect);
    
    % pop the current matrix stack
    Screen('glPopMatrix', tex);
 
end

% screen flip
Screen('Flip', tex);

WaitSecs(2);

% get the image matrix from the texture window
img=Screen('GetImage', tex);

% write it to file (the img matrix returns a 3-D matrix with the Z-axis
% representing RGB. We only need one of these, because we're generating
% grayscale images anyway, so I just write the first layer).
imwrite(img(:,:,1), 'AJL_texture.tiff', 'tiff')

% close everything
sca; clear all;