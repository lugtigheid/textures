clear all;
% close all;

% Create textures for printing
whichIm = 1;    %1 = high contrast, 2 = low contrast
cmImage = 10;   %Specify size of image in centimeters
numDisks = 1;    % number of discs in total

% Dimensions of screen, so we can create image in cm
scr_Num = max(max(Screen('Screens')));
[w_pix, h_pix]=Screen('windowsize', scr_Num);
[w_mm, h_mm]=Screen('DisplaySize', scr_Num);

% calculate the number of pixels per cm
pixpercm = mean([w_pix/w_mm h_pix/h_mm])*10;

% these are in pixels now (the size)
sze = round(cmImage * pixpercm);

% why make this an even number?
sze2 = floor(sze/2) * 2;

diamMin = 1; %Diameter lower limit
diamLim = round(sze/5); %Diameter upper limit

% why multiply here - is already zeros. Is this just pre-allocation?
% am guessing this was to set the background to grey? Doesn't work like
% this (instead this just creates a black background).
bigImage =  0.5 * ones(sze,sze);

% we're going to draw this into a figure
figure;

% cycle through the number of discs. 
for aa = 1:numDisks
    
    % these are values in pixels - randomly sampled from 
    diamVal = randsample(diamMin:diamLim,1);   % Axis 1 radius
    diamVal2 = randsample(diamMin:diamLim,1);  % Axis 2 radius
    
    %Create filter image, use 2D Butterworth filter for each element
    n1 = diamVal; n2 = diamVal2;
    blurval = min(round(diamVal/3), diamMin/2);
    
    
    % this does the butterworth filter
    h1 = psf2([n2 n1], 90, 0, n1-blurval, n2-blurval, 2);
    
    % scale these data
    h = scaledata(h1,0,1);
    
    % Rotating the output makes the size of the matrix change, so diamVal is different, see diamValRot
    theFilter = imrotate(h, rand(1,1)*90, 'bilinear');
    
    % Determine how much of disk will be on image
    diamValRotX = round(size(theFilter,2));
    diamValRotY = round(size(theFilter,1));
    
    % Find location to place element
    topLeftX = randsample(-diamValRotX:sze,1);
    topLeftY = randsample(-diamValRotY:sze,1);
    botRightX = topLeftX + diamValRotX - 1;
    botRightY = topLeftY + diamValRotY - 1;
    
    if(topLeftX<1)  % X is too left
        startX = 1;
        filtStartX = -topLeftX + 2;
    else
        startX = topLeftX;
        filtStartX = 1;
    end
    if(topLeftY<1) % Y is too high
        startY = 1;
        filtStartY = -topLeftY + 2;
    else
        startY = topLeftY;
        filtStartY = 1;
    end
    if(botRightX>sze) % X is too right
        endX = sze;
        filtEndX = diamValRotX - (botRightX - sze);
    else
        endX = botRightX;
        filtEndX = diamValRotX;
    end;
    if(botRightY>sze) % Y is too low
        endY = sze;
        filtEndY = diamValRotY - (botRightY - sze);
    else
        endY = botRightY;
        filtEndY = diamValRotY;
    end;
    
    % this is a patch the size of the ellipse 
    oldPatch = bigImage(startY:endY,startX:endX);
    
    % this is a filter (or a mask) for the new ellipse
    newFilter = theFilter(filtStartY:filtEndY,filtStartX:filtEndX);

    % we have to change the way we incorporate the new into the old
    figure;
    subplot(221);
    imshow(oldPatch);
    
    % this is the limit for fringing
    f = newFilter > 0.0;
    
    subplot(222);
    imshow(newFilter);
    
    pPatch = zeros(size(newFilter,1), size(newFilter,2))
    subplot(223);
    
    imshow(pPatch)
   
    subplot(224)
    imshow(
    
    % Multiply filter by existing patch
    % bothPatch = (oldPatch./(1+newFilter)) + (rand(1))*newFilter;
     
    %Add to image
    % bigImage(startY:endY,startX:endX) = bothPatch;
    
end

% imshow(bigImage);
if whichIm ==1
    im = bigImage;
    saveStr = 'im_Disks_HC.jpg';
    imwrite(im,saveStr,'jpg');
else
    im = scaledata(im,.33,.66);
    saveStr = 'im_Disks_LC.jpg';
    imwrite(im,saveStr,'jpg');
end

